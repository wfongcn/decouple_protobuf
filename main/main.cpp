#include "Person.pb.h"
#include "person2.hpp"
#include "person2_impl.hpp"
#include <libapi2.hpp>

#include <dlfcn.h>

#include <iostream>
#include <memory>
using namespace std;

void test(CALLBACK);

int main(int argc, char** argv)
{
  if (argc < 2) {
    cerr << "Usage: " << argv[0] << " <lib path>" << endl;
    return EXIT_FAILURE;
  }

  // load user's library
  auto lib_descriptor = [&]() {
    auto deleter = [](auto handle){dlclose(handle);};
    return unique_ptr<void, decltype(deleter)>{dlopen(argv[1], RTLD_LAZY), deleter};
  }();
  if (lib_descriptor == nullptr) {
    cout << dlerror() << endl;
    return EXIT_FAILURE;
  }

  // retrive the callback method
  auto callback = (CALLBACK)dlsym(lib_descriptor.get(), "consume");
  if (callback == nullptr) {
    cout << dlerror() << endl;
    return EXIT_FAILURE;
  }

  // send data to client
  test(callback);

  return EXIT_SUCCESS;
}

void test(CALLBACK callback)
{
  cout << "First person:\n------" << endl;
  // construct a protobuf object
  ::person::api::Person first;
  first.set_id(100);
  first.set_name("Hello");
  // package protobuf object to a wrapper object
  ::person::api2::PersonImpl personImpl(&first);
  // pass wrapper object to the user api
  ::person::api2::Person first2(&personImpl);
  // call the callback function to send the object to user
  callback(first2); // <- user will implement it.

  cout << endl << "Second person:\n------" << endl;
  // construct a protobuf object
  ::person::api::Person second;
  second.set_id(200);
  second.set_name("World");
  second.set_type(::person::api::Person_Type_PERSON_BASIC_INFO);
  ::person::api::PersonBasicInfo* info = second.MutableExtension(::person::api::PersonBasicInfo::type);
  info->set_age(88);
  info->set_address("Sun");
  info->set_phone("123");
  info->set_phone2("456");
  // package protobuf object to a wrapper object
  ::person::api2::PersonBasicInfoImpl infoImpl(&second);
  // pass wrapper object to the user api
  ::person::api2::PersonBasicInfo info2(&infoImpl);
  // call the callback function to send the object to user
  callback(info2); // <- user will implement it.
}
