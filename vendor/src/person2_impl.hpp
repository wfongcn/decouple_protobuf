#pragma once

#include <string>
#include <cstdint>

namespace person { namespace api {
  class Person;
  class PersonBasicInfo;
}}

namespace person { namespace api2 {

//--------------------------------------
// enums
//--------------------------------------
enum PersonImpl_Type {
  PersonImpl_Type_PERSON_BASIC_INFO = 100,
  PersonImpl_Type_UNKNOWN
};

//--------------------------------------
// class PersonImpl
//--------------------------------------
class PersonImpl
{
public:
  PersonImpl(const ::person::api::Person* obj);
  virtual ~PersonImpl() {}

  PersonImpl_Type type() const;
  uint32_t id() const;
  const std::string& name() const;

private:
  const ::person::api::Person* mpPerson = nullptr;
};

//--------------------------------------
// class PersonBasicInfoImpl
//--------------------------------------
class PersonBasicInfoImpl : public PersonImpl
{
public:
  PersonBasicInfoImpl(const ::person::api::Person* obj);
  virtual ~PersonBasicInfoImpl() {}

  uint32_t age() const;
  const std::string& address() const;
  const std::string& phone() const;

private:
  const ::person::api::PersonBasicInfo* mpPersonBasicInfo = nullptr;
};

}} // namespace person { namespace api2 {
