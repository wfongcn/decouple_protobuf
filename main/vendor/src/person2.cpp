#include "person2.hpp"
#include "person2_impl.hpp"
#include <stdexcept>

namespace person { namespace api2 {
//--------------------------------------
// class Person
//--------------------------------------
Person::Person(const ::person::api2::PersonImpl* ptr)
: mpPersonImpl(ptr) {
  sHasBits[0] = 0x7;

  if (mpPersonImpl == nullptr) {
    throw std::runtime_error("Cannot initialize person::api2::Person.");
  }
}

Person::~Person() {
}

Person_Type Person::type_internal() const {
  try {
    return (Person_Type)mpPersonImpl->type();
  } catch (const std::runtime_error& e) {
    return (Person_Type)PersonImpl_Type_UNKNOWN;
  }
}

uint32_t Person::id_internal() const {
  try {
    return mpPersonImpl->id();
  } catch (const std::runtime_error& e) {
    throw std::runtime_error("has no Person.id attribute.");
  }
}

const std::string& Person::name_internal() const {
  try {
    return mpPersonImpl->name();
  } catch (const std::runtime_error& e) {
    throw std::runtime_error("has no Person.name attribute.");
  }
}

//--------------------------------------
// class PersonBasicInfo
//--------------------------------------
PersonBasicInfo::PersonBasicInfo(const ::person::api2::PersonImpl* ptr)
: Person(ptr) {
  sHasBits[0] = 0xF;

  if (ptr == nullptr) {
    throw std::runtime_error("Cannot initialize person::api2::PersonBasicInfo.");
  }

  mpPersonBasicInfoImpl = dynamic_cast<const ::person::api2::PersonBasicInfoImpl*>(ptr);
  if (mpPersonBasicInfoImpl == nullptr) {
    throw std::runtime_error("Cannot initialize person::api2::PersonBasicInfo.");
  }
}

PersonBasicInfo::~PersonBasicInfo() {
}

uint32_t PersonBasicInfo::age_internal() const {
  try {
    return mpPersonBasicInfoImpl->age();
  } catch (const std::runtime_error& e) {
    throw std::runtime_error("has no PersonBasicInfo.age attribute.");
  }
}

const std::string& PersonBasicInfo::address_internal() const {
  try {
    return mpPersonBasicInfoImpl->address();
  } catch (const std::runtime_error& e) {
    throw std::runtime_error("has no PersonBasicInfo.address attribute.");
  }
}

const std::string& PersonBasicInfo::phone_internal() const {
  try {
    return mpPersonBasicInfoImpl->phone();
  } catch (const std::runtime_error& e) {
    throw std::runtime_error("has no PersonBasicInfo.phone attribute.");
  }
}

const std::string& PersonBasicInfo::phone2_internal() const {
  try {
    return mpPersonBasicInfoImpl->phone2();
  } catch (const std::runtime_error& e) {
    throw std::runtime_error("has no PersonBasicInfo.phone2 attribute.");
  }
}

}} // namespace person { namespace api2 {
