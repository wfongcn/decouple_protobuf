#pragma once

#include <type_traits>
#include <variant>

using var_t = std::variant<const ::person::api2::Person, const ::person::api2::PersonBasicInfo>;
using CALLBACK = void(*)(const var_t& var);
