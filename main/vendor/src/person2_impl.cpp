#include "person2_impl.hpp"
#include <stdexcept>
#include "Person.pb.h"

namespace person { namespace api2 {
//--------------------------------------
PersonImpl::PersonImpl(const ::person::api::Person* ptr) : mpPerson(ptr) {
  if (ptr == nullptr) {
    throw std::runtime_error("Cannot get ::person::api::Person.");
  }
}

PersonImpl_Type PersonImpl::type() const {
  if (!mpPerson->has_type()) {
    throw std::runtime_error("has no Person.type attribute.");
  }
  return (PersonImpl_Type)mpPerson->type();
}

uint32_t PersonImpl::id() const {
  if (!mpPerson->has_id()) {
    throw std::runtime_error("has no Person.id attribute.");
  }
  return mpPerson->id();
}

const std::string& PersonImpl::name() const {
  if (!mpPerson->has_name()) {
    throw std::runtime_error("has no Person.name attribute.");
  }
  return mpPerson->name();
}

//--------------------------------------

PersonBasicInfoImpl::PersonBasicInfoImpl(const ::person::api::Person* ptr)
: PersonImpl(ptr) {
  if (ptr == nullptr) {
    throw std::runtime_error("Cannot get ::person::api::PersonBasicInfo.");
  }

  mpPersonBasicInfo = &ptr->GetExtension(::person::api::PersonBasicInfo::type);
  if (mpPersonBasicInfo == nullptr) {
    throw std::runtime_error("Cannot get ::person::api::PersonBasicInfo.");
  }
}

uint32_t PersonBasicInfoImpl::age() const {
  if (!mpPersonBasicInfo->has_age()) {
    throw std::runtime_error("has no PersonBasicInfo.age attribute.");
  }

  return mpPersonBasicInfo->age();
}

const std::string& PersonBasicInfoImpl::address() const {
  if (!mpPersonBasicInfo->has_address()) {
    // TODO: maybe we can return empty string instead of exception???
    throw std::runtime_error("has no PersonBasicInfo.address attribute.");
  }

  return mpPersonBasicInfo->address();
}

const std::string& PersonBasicInfoImpl::phone() const {
  if (!mpPersonBasicInfo->has_phone()) {
    // TODO: maybe we can return empty string instead of exception???
    throw std::runtime_error("has no PersonBasicInfo.phone attribute.");
  }

  return mpPersonBasicInfo->phone();
}

const std::string& PersonBasicInfoImpl::phone2() const {
  if (!mpPersonBasicInfo->has_phone2()) {
    // TODO: maybe we can return empty string instead of exception???
    throw std::runtime_error("has no PersonBasicInfo.phone2 attribute.");
  }

  return mpPersonBasicInfo->phone2();
}

}} // namespace person { namespace api2 {
