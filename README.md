# decouple_protobuf

## Getting started

This C++ project want to show a example of decouple protobuf API from own project.

There are 3 parts in this project,
* main/ -> Can see all of vendor's implementation, it creates all data and then send the data to the client by callback function, the client is a shared dynamic library loaded by main.cpp.
* vendor/  -> Uses the protobuf to create the data structure, and provide a non-protobuf API to the client by a header file (vendor/include/person2.hpp) and an shared library (vendor/lib/libpersonapi.so).
* client/  -> Implement the callback function, and call the non-protobuf API to parse the data.

The data structure in the vendor can be extended more data, we expect the client can work with different version of vendors.

Non-functional requirement:
- The API provided by vendor is forward and backward compatibility.

## How to use

```
git clone https://gitlab.com/wfongcn/decouple_protobuf.git
cd decouple_protobuf
make install
./run.sh
```

## Needs
* A Linux environment
* A g++ compiler can support C++17
* google protobuf2
* make tool

## TODOs
* How to add version information in the shared library
