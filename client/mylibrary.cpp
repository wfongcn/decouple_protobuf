#include <person2.hpp>
#include <libapi2.hpp>

#include <variant>
#include <iostream>
using namespace std;

auto handlePerson = [](const ::person::api2::Person& person) {
  cout << "id: " << person.id() << endl;
  cout << "name: " << person.name() << endl;
};

auto handlePersonInfo = [](const ::person::api2::PersonBasicInfo& info) {
  cout << "id: " << info.id() << endl;
  cout << "name: " << info.name() << endl;

  cout << "age: " << info.age() << endl;
  cout << "address: " << info.address() << endl;
};

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

extern "C" void consume(const var_t& var) {
  std::visit(overloaded {
    handlePerson,
    handlePersonInfo
  }, var);
}

