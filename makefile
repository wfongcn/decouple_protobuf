all: clean
	cd vendor/src;make;cd ../..
	cd client;make;cd ..
	cd main;make;cd ..

clean:
	cd vendor/src;make clean;cd ../..
	cd client;make clean;cd ..
	cd main;make clean;cd ..

.PHONY: all clean
