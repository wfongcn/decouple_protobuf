#pragma once

#include <string>
#include <cstdint>
#include <stdexcept>

//--------------------------------------
// API
//--------------------------------------
namespace person { namespace api2 {
class PersonImpl;
class PersonBasicInfoImpl;

enum Person_Type {
  Person_Type_PERSON_BASIC_INFO = 100,
  Person_Type_UNKNOWN
};

//--------------------------------------
// class Person
//--------------------------------------
class Person
{
public:
  Person(const ::person::api2::PersonImpl* ptr);
  virtual ~Person();

  inline Person_Type type() const {
    if (sHasBits[0] & 0x1) {
      return type_internal();
    } else {
      return Person_Type_UNKNOWN;
    }
  }

  inline uint32_t id() const {
    if (sHasBits[0] & 0x2) {
      return id_internal();
    } else {
      throw std::runtime_error("has no Person.id.");
    }
  }

  inline const std::string& name() const {
    if (sHasBits[0] & 0x4) {
      return name_internal();
    } else {
      throw std::runtime_error("has no Person.name.");
    }
  }

private:
  Person_Type type_internal() const;
  uint32_t id_internal() const;
  const std::string& name_internal() const;

private:
  inline static uint32_t sHasBits[1];
  const ::person::api2::PersonImpl* mpPersonImpl = nullptr;
};

//--------------------------------------
// class PersonBasicInfo
//--------------------------------------
class PersonBasicInfo : public Person
{
public:
  PersonBasicInfo(const ::person::api2::PersonImpl* ptr);
  virtual ~PersonBasicInfo();

  inline uint32_t age() const {
    if (sHasBits[0] & 0x1) {
      return age_internal();
    } else {
      throw std::runtime_error("has no PersonBasicInfo.age.");
    }
  }

  inline const std::string& address() const {
    if (sHasBits[0] & 0x2) {
      return address_internal();
    } else {
      throw std::runtime_error("has no PersonBasicInfo.address.");
    }
  }

  inline const std::string& phone() const {
    if (sHasBits[0] & 0x4) {
      return phone_internal();
    } else {
      throw std::runtime_error("has no PersonBasicInfo.phone.");
    }
  }

  inline const std::string& phone2() const {
    if (sHasBits[0] & 0x8) {
      return phone2_internal();
    } else {
      throw std::runtime_error("has no PersonBasicInfo.phone2.");
    }
  }

private:
  uint32_t age_internal() const;
  const std::string& address_internal() const;
  const std::string& phone_internal() const;
  const std::string& phone2_internal() const;

private:
  inline static uint32_t sHasBits[1];
  const ::person::api2::PersonBasicInfoImpl* mpPersonBasicInfoImpl = nullptr;
};

}} // namespace person { namespace api2 {

